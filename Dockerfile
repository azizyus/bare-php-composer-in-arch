


FROM archlinux/base

RUN pacman -Sy
RUN pacman -S php nano composer --noconfirm
RUN sed -i '/^/s/;extension=iconv/extension=iconv/' /etc/php/php.ini
RUN mkdir "/project"
